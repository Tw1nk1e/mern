import { GET_ITEMS, ADD_ITEM, DELETE_ITEM } from "../actions/types";

import { reject } from "ramda";

const initialState = {
  items: []
};

const apiReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEMS:
      return { items: [...action.payload] };
    case ADD_ITEM:
      return { items: [...state.items, action.payload] };
    case DELETE_ITEM:
      const isDelItem = i => i._id === action.payload;
      return { items: [...reject(isDelItem, state.items)] };
    default:
      return state;
  }
};

export default apiReducer;
