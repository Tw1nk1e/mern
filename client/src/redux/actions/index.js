import axios from "axios";
import { GET_ITEMS, ADD_ITEM, DELETE_ITEM } from "./types";

export const getItems = () => dispatch => {
  axios.get("http://localhost:5000/api/").then(res =>
    dispatch({
      type: GET_ITEMS,
      payload: res.data
    })
  );
};

export const addItem = item => dispatch => {
  axios.post("http://localhost:5000/api/", item).then(res =>
    dispatch({
      type: ADD_ITEM,
      payload: res.data
    })
  );
};

export const deleteItem = id => dispatch => {
  axios.delete(`http://localhost:5000/api/${id}`).then(res =>
    dispatch({
      type: DELETE_ITEM,
      payload: id
    })
  );
};
