import styled from 'styled-components';

import { colors } from './global/constants';

export const Wrapper = styled.form`
  flex-wrap: 'nowrap';
  flex-direction: 'row';
  padding: 10px 0px 10px 20px;
`;

export const Input = styled.input`
  width: 200ps;
  height: 40px;
  border: none;
  margin-right: 10px;
  border-radius: 5px;
`;

export const SubmitButton = styled.input`
  width: 100px;
  height: 40px;
  border: none;
  color: white;
  border-radius: 5px;
  background: ${colors.buttonRed};
`;
