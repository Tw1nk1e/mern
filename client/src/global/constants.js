export const colors = {
  buttonRed: '#ED5E68',
  background: '#363B59'
};

export const closeIcon = 'https://img.icons8.com/ios/50/000000/delete-sign.png';
