import React, { Component } from "react";
import { connect } from "react-redux";

import Item from "./components/Item";

import { getItems, addItem, deleteItem } from "../src/redux/actions/index";

import { Wrapper, Input, SubmitButton } from "./styles";
class App extends Component {
  state = {
    inputValue: ""
  };

  componentDidMount = () => {
    this.props.getItems();
  };

  handleAddItem = async name => {
    const item = {
      name
    };
    this.props.addItem(item);
  };

  handleDeleteItem = id => {
    this.props.deleteItem(id);
  };

  handleInputChange = event => {
    this.setState({
      inputValue: event.target.value
    });
  };

  handleSubmit = event => {
    const { inputValue } = this.state;
    event.preventDefault();
    this.setState({
      inputValue: ""
    });
    this.handleAddItem(inputValue);
  };

  render() {
    const {
      props: { items },
      state: { inputValue }
    } = this;

    return (
      <>
        <Wrapper onSubmit={this.handleSubmit}>
          <Input
            type="text"
            value={inputValue}
            placeholder="Введите..."
            onChange={this.handleInputChange}
          />
          <SubmitButton type="submit" value="Отправить" />
        </Wrapper>
        {items.map(item => {
          return (
            <Item
              item={item}
              key={item._id}
              deleteItem={this.handleDeleteItem}
            />
          );
        })}
      </>
    );
  }
}

const mapStateToProps = state => ({
  items: state.apiReducer.items
});

export default connect(
  mapStateToProps,
  { getItems, addItem, deleteItem }
)(App);
