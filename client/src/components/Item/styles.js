import styled from "styled-components";

export const Container = styled.section`
  display: flex;
  padding: 10px;
  background: white;
  position: relative;
  border-radius: 10px;
  align-items: center;
  flex-direction: row;
  transition: all 0.2s;
  margin: 0px 20px 5px 20px;
  justify-content: space-between;

  :after {
    transition: opacity 0.2s ease-in-out;
  }

  :hover {
    -webkit-transform: scale(1.02, 1);
    transform: scale(1.02, 1);
  }
`;

export const DeleteButton = styled.img`
  opacity: 0;
  width: 35px;
  height: 35px;
  border-radius: 5px;

  transition: all 0.2s;

  :hover {
    width: 40px;
    height: 40px;
  }

  ${Container}:hover & {
    opacity: 1;
  }
`;

export const Wrapper = styled.div``;

export const Title = styled.h1`
  color: black;
  font-size: 1.5em;
`;

export const Date = styled.h2`
  color: black;
  font-size: 1em;
`;
