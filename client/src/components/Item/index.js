import React from "react";

import { closeIcon } from "../../global/constants";
import { Container, Wrapper, Title, Date, DeleteButton } from "./styles";

export default function Item({ item, deleteItem }) {
  return (
    <Container>
      <Wrapper>
        <Title>{item.name}</Title>
        <Date>{item.createdAt}</Date>
      </Wrapper>
      <DeleteButton
        alt="X"
        src={closeIcon}
        onClick={() => deleteItem(item._id)}
      />
    </Container>
  );
}
