const express = require("express");
const mongodb = require("mongodb");
const { mongoLogin } = require("../config/constants");

const router = express.Router();

// Get Posts
router.get("/api/", async (req, res) => {
  const posts = await loadPostsCollection();
  res.send(await posts.find({}).toArray());
});

// Add Post
router.post("/api/", async (req, res) => {
  const posts = await loadPostsCollection();
  const item = {
    name: req.body.name,
    createdAt: new Date()
  };

  await posts.insertOne(item);
  res.status(201).send(item);
});

// Delete Post
router.delete("/api/:id", async (req, res) => {
  const posts = await loadPostsCollection();
  const itemId = { id: req.body.id };

  await posts.deleteOne(itemId);
  res.status(202).send("Item was delete...");
});

async function loadPostsCollection() {
  const client = await mongodb.MongoClient.connect(mongoLogin, {
    useNewUrlParser: true
  });
  return client.db("TestDataBase").collection("TestCollection");
}

module.exports = router;
